function setToString(G, set) {
    let s = "{";
    set.forEach(v => { s += G.node(v).label() + ", " });
    return s + "}";
}

function dictToString(G, dict) {
    let s = "<table class='dict'>";
    s += "<thead><tr>";
    Object.keys(dict).forEach(v => {s += "<th>" + G.node(v).label() + "</th>";});
    s += "</thead></tr><tbody><tr>";
    Object.keys(dict).forEach(v => {s += "<td class='centered'>" + ((dict[v] === null)?"-":((dict[v] === Number.POSITIVE_INFINITY)?"+&infin;":dict[v])) + "</td>";});
    return s + "</tr></tboby></table>";
}

function xileToString(G, xile) {
    let s = "[";
    xile.__structure.forEach(v => { 
        if (typeof v === "object"){
            s += "(" + G.node(v[0]).label() + ", " + setToString(G, v[1]) + "), "; 
        } else {
            s += G.node(v).label() + ", ";
        }
    });
    return s + "]";
}

function minCout(e, couts){
    let mini = null;
    e.forEach(n => {
        if (mini === null){
            mini = n;
        } else {
            if (couts[n] < couts[mini])
                mini = n;
        }
    });
    return mini;
}
const algos = {
    "cheminMinCout": {
        "help": "<h1>Algorithme - Recherche d'un chemin de coût minimal entre 2 sommets</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme s'applique sur un graphe pondéré et nécessite un sommet de départ et d'arrivée</p><p>Sortie : Une liste de sommets modélisant un chemin de coût minimal s'il existe un chemin entre le sommet de départ et d'arrivée, Vide sinon.</p><h2>Principe</h2><p>L'algorithme a été publié par Edsger Dijkstra en 1959 dans la revue Numerische Mathematik (<a target='_blank' href='http://www.cs.yale.edu/homes/lans/readings/routing/dijkstra-routing-1959.pdf'>Article accessible ici</a>).</p>",
        "parameters": ["departContainer", "arriveeContainer"],
        "function": function (G, depart, arrivee, e, steps = [], resultat = []) {
            let nodes = [...G.nodes()].filter(n => !n.estVide());
            let couts = {};
            let predecesseur = {};
            nodes.forEach(n => {
                couts[n.id] = Number.POSITIVE_INFINITY;
                predecesseur[n.id] = null;
            });
            couts[depart] = 0;
            G.node(arrivee).set("color", "blue");
            steps.push({ "line": 1, "départ": G.node(depart).label(), "arrivée": G.node(arrivee).label(), "couts" : dictToString(G, couts), "predecesseur" : dictToString(G, predecesseur), "graph" : G.toDot()});
            let E = new Set();
            steps.push({ "line": 2, "e" : setToString(G, E)});
            
            nodes.forEach(n => E.add(n.id));
            steps.push({ "line": 3, "e" : setToString(G, E)});
            steps.push({ "line": 4});
            while (E.size > 0){
                steps.push({ "line": 5});
                let t = minCout(E, couts);
                G.node(t).set("color", "red");
                G.node(t).set("style", "filled");
                G.node(t).set("fillcolor", "lightgray");
                steps.push({ "line": 6, "t" : G.node(t).label(), "graph" : G.toDot()});
                E.delete(t);
                steps.push({ "line": 7, "e" : setToString(G, E)});
                G.neighbor(t).forEach(v => {
                    if (!G.node(v).estVide()){
                        G.edge(t, v).set("color", "red");
                        steps.push({ "line": 8, "v" : G.node(v).label(), "graph" : G.toDot()});
                        if (couts[v] > couts[t] + G.edge(t, v).value()){
                            steps.push({ "line": 9});
                            couts[v] = couts[t] + G.edge(t, v).value();
                            steps.push({ "line": 10, "couts" : dictToString(G, couts)});
                            predecesseur[v] = t;
                            steps.push({ "line": 7, "predecesseur" : dictToString(G, predecesseur)});
                        }
                    }
                });
                steps.push({ "line": 4});
            }
            steps.push({ "line": 11});
            let chemin = new Pile();
            let noeud = arrivee;
            
            while (noeud != depart){
                G.node(noeud).set("fillcolor", "black");
                G.node(noeud).set("fontcolor", "white");
                chemin.empiler(noeud);
                noeud = predecesseur[noeud];
                G.edge(chemin.sommet(), noeud).set("color", "blue");
            }
            G.node(depart).set("fillcolor", "black");
            G.node(depart).set("fontcolor", "white");
            chemin.empiler(depart);

            steps.push({ "line": 12, "graph": G.toDot(), "chemin": setToString(G, chemin.__structure.reverse())});
        },
        "descriptif": [
            "<i>couts</i>, <i>predecesseur</i> &leftarrow; Initialisation(G, <i>départ</i>)",
            "<i>e</i> &leftarrow; Créer un ensemble",
            "Ajouter tous les sommets de <i>G</i> dans <i>e</i>",
            "Tant que <i>e</i> est non vide",
            "&nbsp;&nbsp;<i>t</i> &leftarrow;  Trouver le sommet de coût minimal de <i>e</i>", 
            "&nbsp;&nbsp;Retirer <i>t</i> de <i>e</i>",
            "&nbsp;&nbsp;Pour chaque sommet voisin <i>v</i> de <i>t</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;Si couts[v] > couts[t] + poids(t, v) alors",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;couts[v] &leftarrow; couts[t] + poids(t, v)",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;predecesseur[v] &leftarrow; <i>t</i>",
            "<i>chemin</i> &leftarrow; Prédécesseurs successifs de <i>arrivée</i> au <i>départ</i>"
        ],
        "variables": ["départ", "arrivée", "couts", "predecesseur", "e", "t", "v", "chemin"]
    },
    "cheminMinLongueur": {
        "help": "<h1>Algorithme - Recherche d'un chemin de longueur minimale entre 2 sommets</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme s'applique sur un graphe et nécessite un sommet de départ et d'arrivée</p><p>Sortie : Une liste de sommets modélisant un chemin de longueur minimale s'il existe un chemin entre le sommet de départ et d'arrivée, Vide sinon.</p><h2>Principe</h2><p>L'algorithme s'inspire du parcours en largeur, i.e en traitant les sommets par distance.</p><p>À chaque traitement d'un sommet, l'algorithme stocke le chemin qui a permis d'arriver jusqu'à lui.</p><p>Si le sommet traité est le sommet d'arrivée, le chemin est renvoyé.</p><p>Si le sommet d'arrivée n'est pas traité, i.e il n'existe pas de chemin entre le sommet de départ et le sommet d'arrivée. L'algorithme renvoie Vide.</p><h2>Codes couleur</h2><ul><li>Le sommet cerclé de bleu <span class=\"node blue\"></span> symbolise le sommet d'arrivée,</li><li>Un sommet cerclé de noir <span class=\"node black\"></span> symbolise un élément non encore découvert par l'algorithme,</li><li>Un sommet cerclé de rouge <span class=\"node red\"></span> symbolise un élément découvert, i.e qui sera traité ultérieurement par l'algorithme,</li><li>Un sommet en gris <span class=\"node red plain-gray\"></span> symbolise un sommet dans le chemin.</li></ul></p>",
        "parameters": ["departContainer", "arriveeContainer"],
        "function": function (G, depart, arrivee, e, steps = [], resultat = []) {
            const sommet = G.node(depart);
            sommet.set("color", "red");
            G.node(arrivee).set("color", "blue");
            steps.push({ "line": 1, "départ": sommet.label(), "arrivée": G.node(arrivee).label(), "graph": G.toDot() });
            
            let f = new File();
            steps.push({ "line": 2, "f": xileToString(G, f)});
            f.enfiler([depart, []]);
            steps.push({ "line": 3, "f": xileToString(G, f)});
            while (!f.est_vide()){
                steps.push({ "line": 4});
                let s, chemin;
                [s, chemin] = f.defiler();
                steps.push({ "line": 5, "s" : G.node(s).label(), "f": xileToString(G, f), "chemin" : setToString(G, chemin)});
                chemin.push(s);
                steps.push({ "line": 6, "chemin" : setToString(G, chemin)});
                if (s == arrivee){
                    steps.push({ "line": 7});
                    chemin.forEach((n, i, a) => {
                        G.node(n).set("style", "filled");
                        G.node(n).set("fillcolor", "lightgray");
                        if (i > 0)
                            G.edge(n, a[i - 1]).set("color", "blue");
                    });
                    steps.push({ "line": 12, "graph": G.toDot()});
                    return chemin;
                }
                steps.push({ "line": 8});
                G.neighbor(s).forEach(v => {
                    if (!G.node(v).estVide()) {
                        G.node(v).set("color", "red");
                        if (G.edge(s, v) !== undefined)
                            G.edge(s, v).set("color", "red");
                        steps.push({ "line": 9, "v" : G.node(v).label(), "graph" : G.toDot()});
                        if (chemin.indexOf(v) == -1){
                            steps.push({ "line": 10});
                            f.enfiler([v, [...chemin]]);
                            steps.push({ "line": 8, "f": xileToString(G, f)});
                        }
                    }
                });
            }
            steps.push({ "line": 11});
            steps.push({ "line": 12, "chemin" : "Vide"});
            return [];
        },
        "descriptif": [
            "<i>f</i> &larr; Créer une file",
            "Enfiler (<i>départ</i>, []) dans <i>f</i>",
            "Tant que <i>f</i> est non vide",
            "&nbsp;&nbsp;<i>s</i>, <i>chemin</i> &larr; Défiler <i>f</i>",
            "&nbsp;&nbsp;<i>chemin</i> &leftarrow; <i>chemin</i> &#8746; <i>s</i>",
            "&nbsp;&nbsp;Si le sommet <i>s</i> est égal au sommet <i>arrivée</i> alors",
            "&nbsp;&nbsp;&nbsp;&nbsp;Renvoyer chemin",
            "&nbsp;&nbsp;Pour tous les sommets adjacents <i>v</i> de <i>s</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;Si le sommet <i>v</i> n'est pas dans <i>chemin</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enfiler le sommet (<i>v</i>, chemin) dans <i>f</i>",
            "Renvoyer Vide"
        ],
        "variables": ["départ", "arrivée", "f", "s", "v", "chemin"]
    },
    "chemin": {
        "help": "<h1>Algorithme - Recherche d'un chemin entre 2 sommets</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme s'applique sur un graphe et nécessite un sommet de départ et d'arrivée</p><p>Sortie : Une liste de sommets modélisant un chemin s'il existe un chemin entre le sommet de départ et d'arrivée, Vide sinon.</p><h2>Principe</h2><p>L'algorithme s'inspire du parcours en profondeur.</p><p>À chaque traitement d'un sommet, l'algorithme stocke le chemin qui a permis d'arriver jusqu'à lui.</p><p>Si le sommet traité est le sommet d'arrivée, le chemin est renvoyé.</p><p>Si le sommet d'arrivée n'est pas traité, i.e il n'existe pas de chemin entre le sommet de départ et le sommet d'arrivée. L'algorithme renvoie Vide.</p><h2>Codes couleur</h2><ul><li>Le sommet cerclé de bleu <span class=\"node blue\"></span> symbolise le sommet d'arrivée,</li><li>Un sommet cerclé de noir <span class=\"node black\"></span> symbolise un élément non encore découvert par l'algorithme,</li><li>Un sommet cerclé de rouge <span class=\"node red\"></span> symbolise un élément découvert, i.e qui sera traité ultérieurement par l'algorithme,</li><li>Un sommet en gris <span class=\"node red plain-gray\"></span> symbolise un sommet dans le chemin.</li></ul></p>",
        "parameters": ["departContainer", "arriveeContainer"],
        "function": function (G, depart, arrivee, e, steps = [], resultat = []) {
            const sommet = G.node(depart);
            sommet.set("color", "red");
            G.node(arrivee).set("color", "blue");
            steps.push({ "line": 1, "départ": sommet.label(), "arrivée": G.node(arrivee).label(), "graph": G.toDot() });
            
            let p = new Pile();
            steps.push({ "line": 2, "p": xileToString(G, p)});
            p.empiler([depart, []]);
            steps.push({ "line": 3, "p": xileToString(G, p)});
            while (!p.est_vide()){
                steps.push({ "line": 4});
                let s, chemin;
                [s, chemin] = p.depiler();
                steps.push({ "line": 5, "s" : G.node(s).label(), "p": xileToString(G, p), "chemin" : setToString(G, chemin)});
                chemin.push(s);
                steps.push({ "line": 6, "chemin" : setToString(G, chemin)});
                if (s == arrivee){
                    steps.push({ "line": 7});
                    chemin.forEach((n, i, a) => {
                        G.node(n).set("style", "filled");
                        G.node(n).set("fillcolor", "lightgray");
                        if (i > 0)
                            G.edge(n, a[i - 1]).set("color", "blue");
                    });
                    steps.push({ "line": 12, "graph": G.toDot()});
                    return chemin;
                }
                steps.push({ "line": 8});
                G.neighbor(s).forEach(v => {
                    if (!G.node(v).estVide()) {
                        G.node(v).set("color", "red");
                        if (G.edge(s, v) !== undefined)
                            G.edge(s, v).set("color", "red");
                        steps.push({ "line": 9, "v" : G.node(v).label(), "graph" : G.toDot()});
                        if (chemin.indexOf(v) == -1){
                            steps.push({ "line": 10});
                            p.empiler([v, [...chemin]]);
                            steps.push({ "line": 8, "p": xileToString(G, p)});
                        }
                    }
                });
            }
            steps.push({ "line": 11});
            steps.push({ "line": 12, "chemin" : "Vide"});
            return [];
        },
        "descriptif": [
            "<i>p</i> &larr; Créer une pile",
            "Empiler (<i>départ</i>, []) dans <i>p</i>",
            "Tant que <i>p</i> est non vide",
            "&nbsp;&nbsp;<i>s</i>, <i>chemin</i> &larr; Dépiler <i>p</i>",
            "&nbsp;&nbsp;<i>chemin</i> &leftarrow; <i>chemin</i> &#8746; <i>s</i>",
            "&nbsp;&nbsp;Si le sommet <i>s</i> est égal au sommet <i>arrivée</i> alors",
            "&nbsp;&nbsp;&nbsp;&nbsp;Renvoyer chemin",
            "&nbsp;&nbsp;Pour tous les sommets adjacents <i>v</i> de <i>s</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;Si le sommet <i>v</i> n'est pas dans <i>chemin</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Empiler le sommet (<i>v</i>, chemin) dans <i>p</i>",
            "Renvoyer Vide"
        ],
        "variables": ["départ", "arrivée", "p", "s", "v", "chemin"]
    },
    "hauteur": {
        "help": "<h1>Algorithme - Hauteur d'un Arbre</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un arbre binaire.</p><p>Sortie : La hauteur de l'arbre A, i.e la profondeur maximale entre sa racine et ses feuilles.</p><h2>Principe</h2><p>L'algorithme est naturellement récursif.</p><p>Par convention, on considère que la hauteur d'un arbre vide ∆ est -1.</p><p>La hauteur d'un arbre non vide correspond au maximum de la hauteur de ses 2 sous-arbres auquel on ajoute 1 pour la racine courante.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = [], resultat = 0) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "graph": G.toDot() });
            if (sommet.estVide()) {
                steps.push({ "line": 2 });
                return -1;
            } else {
                steps.push({ "line": 3 });
                let n = G.neighbor(s);
                let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];
                let hGauche = -1;
                let hDroite = -1;

                G.edge(s, children[0]).set("color", "red");
                steps.push({ "line": 4, "graph": G.toDot() });
                hGauche = this.function(G, children[0], d, e, steps);
                G.edge(s, children[1]).set("color", "red");
                steps.push({ "line": 5, "s": sommet.label(), "graph": G.toDot(), "hauteur_gauche": hGauche, "hauteur_droit": hDroite });
                hDroite = this.function(G, children[1], d, e, steps);
                steps.push({ "line": 6, "s": sommet.label(), "hauteur_gauche": hGauche, "hauteur_droit": hDroite });

                resultat = 1 + Math.max(hGauche, hDroite);
                sommet.set("style", "filled");
                sommet.set("fillcolor", "lightgray");
                steps.push({ "line": 7, "graph": G.toDot(), "résultat": resultat });
                return 1 + Math.max(hGauche, hDroite);
            }
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est vide alors",
            "&nbsp;&nbsp;Renvoyer -1",
            "Sinon",
            "&nbsp;&nbsp;hauteur_gauche &leftarrow; hauteur(sous-arbre gauche de <i>s</i>)",
            "&nbsp;&nbsp;hauteur_droit &leftarrow; hauteur(sous-arbre droit de <i>s</i>)",
            "&nbsp;&nbsp;Renvoyer 1 + max(hauteur_gauche + hauteur_droit)"
        ],
        "variables": ["s", "hauteur_gauche", "hauteur_droit", "résultat"]
    },
    "taille": {
        "help": "<h1>Algorithme - Taille d'un Arbre</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un arbre binaire.</p><p>Sortie : La taille de l'arbre A, i.e son nombre de noeuds.</p><h2>Principe</h2><p>L'algorithme est naturellement récursif.</p><p>Par convention, on considère que la taille d'un arbre vide ∆ est 0.</p><p>La taille d'un arbre non vide correspond à la taille de ses 2 sous-arbres auquel on ajoute 1 pour la racine courante.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = [], resultat = 0) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "graph": G.toDot() });
            if (sommet.estVide()) {
                steps.push({ "line": 2 });
                return 0;
            } else {
                steps.push({ "line": 3 });
                let n = G.neighbor(s);
                let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];
                let tGauche = 0;
                let tDroite = 0;

                G.edge(s, children[0]).set("color", "red");
                steps.push({ "line": 4, "graph": G.toDot() });
                tGauche = this.function(G, children[0], d, e, steps);
                G.edge(s, children[1]).set("color", "red");
                steps.push({ "line": 5, "s": sommet.label(), "graph": G.toDot(), "taille_gauche": tGauche, "taille_droit": tDroite });
                tDroite = this.function(G, children[1], d, e, steps);
                steps.push({ "line": 6, "s": sommet.label(), "taille_gauche": tGauche, "taille_droit": tDroite });

                resultat = 1 + tGauche + tDroite;
                sommet.set("style", "filled");
                sommet.set("fillcolor", "lightgray");
                steps.push({ "line": 7, "graph": G.toDot(), "résultat": resultat });
                return 1 + tGauche + tDroite;
            }
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est vide alors",
            "&nbsp;&nbsp;Renvoyer 0",
            "Sinon",
            "&nbsp;&nbsp;taille_gauche &leftarrow; taille(sous-arbre gauche de <i>s</i>)",
            "&nbsp;&nbsp;taille_droit &leftarrow; taille(sous-arbre droit de <i>s</i>)",
            "&nbsp;&nbsp;Renvoyer 1 + taille_gauche + taille_droit"
        ],
        "variables": ["s", "taille_gauche", "taille_droit", "résultat"]
    },
    "profondeur": {
        "help": "<h1>Algorithme - Parcours en profondeur</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un graphe.</p><p>Effet de bord : Les sommets sont traités dans l'ordre du parcours en profondeur.</p><h2>Principe</h2><p>C'est un algorithme de base de parcours d'un graphe.</p><p>Le principe est d'explorer chaque chemin depuis le sommet de départ jusqu'à atteindre un cul-de-sac ou avoir visité tous les sommets, puis de revenir au dernier sommet où il était possible de prendre un chemin différent.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = []) {
            const sommet = G.node(s);
            steps.push({ "line": 1, "s": sommet.label() });
            let p = new Pile();
            steps.push({ "line": 2, "p": xileToString(G, p) });
            let E = new Set();
            let resultat = [];
            steps.push({ "line": 3, "e": setToString(G, E) });
            p.empiler(s);
            steps.push({ "line": 4, "p": xileToString(G, p) });
            E.add(s);
            sommet.set("color", "red");
            steps.push({ "line": 5, "e": setToString(G, E), "graph": G.toDot() });
            while (!p.est_vide()) {
                steps.push({ "line": 6 });
                t = p.depiler();
                steps.push({ "line": 7, "p": xileToString(G, p), "t": G.node(t).label() });
                resultat.push(G.node(t).label());
                G.node(t).set("style", "filled");
                G.node(t).set("fillcolor", "lightgray");
                steps.push({ "line": 8, "résultat": resultat.toString(), "graph": G.toDot() });
                G.neighbor(t).forEach(v => {
                    if (!G.node(v).estVide()) {
                        if (G.edge(t, v) !== undefined)
                            G.edge(t, v).set("color", "red");
                        steps.push({ "line": 9, "v": G.node(v).label(), "graph": G.toDot() });
                        if (!E.has(v)) {
                            steps.push({ "line": 10 });
                            p.empiler(v);
                            steps.push({ "line": 11, "p": xileToString(G, p) });
                            E.add(v);
                            G.node(v).set("color", "red");
                            steps.push({ "line": 8, "e": setToString(G, E), "graph": G.toDot() });
                        } else {
                            steps.push({ "line": 8 });
                        }
                    }
                });
                steps.push({ "line": 5 });
            }
            steps.push({ "line": 12 });
        },
        "descriptif": [
            "<i>p</i> &larr; Créer une pile",
            "<i>e</i> &larr; Créer un ensemble",
            "Empiler <i>s</i> dans <i>p</i>",
            "Ajouter <i>s</i> dans <i>e</i>",
            "Tant que <i>p</i> est non vide",
            "&nbsp;&nbsp;<i>t</i> &larr; Dépiler <i>p</i>",
            "&nbsp;&nbsp;Traiter le sommet <i>t</i>",
            "&nbsp;&nbsp;Pour tous les sommets adjacents <i>v</i> de <i>t</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;Si le sommet <i>v</i> n'est pas marqué dans <i>e</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Empiler le sommet <i>v</i> dans <i>p</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ajouter le sommet <i>v</i> dans <i>e</i>"
        ],
        "variables": ["s", "p", "e", "t", "v", "résultat"]
    },
    "largeur": {
        "help": "<h1>Algorithme - Parcours en largeur</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un graphe.</p><p>Effet de bord : Les sommets sont traités dans l'ordre du parcours en largeur.</p><h2>Principe</h2><p>C'est un algorithme de base de parcours d'un graphe.</p><p>Le principe est d'explorer les sommets par distance à un sommet donné. Tout d'abord, les sommets à distance 1, puis 2....jusqu'aux feuilles.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = []) {
            const sommet = G.node(s);
            steps.push({ "line": 1, "s": sommet.label() });
            let f = new File();
            steps.push({ "line": 2, "f": xileToString(G, f) });
            let E = new Set();
            let resultat = [];
            steps.push({ "line": 3, "e": setToString(G, E) });
            f.enfiler(s);
            steps.push({ "line": 4, "f": xileToString(G, f) });
            E.add(s);
            sommet.set("color", "red");
            steps.push({ "line": 5, "e": setToString(G, E), "graph": G.toDot() });
            while (!f.est_vide()) {
                steps.push({ "line": 6 });
                t = f.defiler();
                steps.push({ "line": 7, "f": xileToString(G, f), "t": G.node(t).label() });
                resultat.push(G.node(t).label());
                G.node(t).set("style", "filled");
                G.node(t).set("fillcolor", "lightgray");
                steps.push({ "line": 8, "résultat": resultat.toString(), "graph": G.toDot() });
                G.neighbor(t).forEach(v => {
                    if (!G.node(v).estVide()) {
                        if (G.edge(t, v) !== undefined)
                            G.edge(t, v).set("color", "red");
                        steps.push({ "line": 9, "v": G.node(v).label(), "graph": G.toDot() });
                        if (!E.has(v)) {
                            steps.push({ "line": 10 });
                            f.enfiler(v);
                            steps.push({ "line": 11, "f": xileToString(G, f) });
                            E.add(v);
                            G.node(v).set("color", "red");
                            steps.push({ "line": 8, "e": setToString(G, E), "graph": G.toDot() });
                        } else {
                            steps.push({ "line": 8 });
                        }
                    }
                });
                steps.push({ "line": 5 });
            }
            steps.push({ "line": 12 });
        },
        "descriptif": [
            "<i>f</i> &larr; Créer une file",
            "<i>e</i> &larr; Créer un ensemble",
            "Empiler <i>s</i> dans <i>f</i>",
            "Ajouter <i>s</i> dans <i>e</i>",
            "Tant que <i>f</i> est non vide",
            "&nbsp;&nbsp;<i>t</i> &larr; Défiler <i>f</i>",
            "&nbsp;&nbsp;Traiter le sommet <i>t</i>",
            "&nbsp;&nbsp;Pour tous les sommets adjacents <i>v</i> de <i>t</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;Si le sommet <i>v</i> n'est pas marqué dans <i>e</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enfiler le sommet <i>v</i> dans <i>f</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ajouter le sommet <i>v</i> dans <i>e</i>"
        ],
        "variables": ["s", "f", "e", "t", "v", "résultat"]
    },
    "infixe": {
        "help": "<h1>Algorithme - Parcours infixe</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un arbre binaire.</p><p>Effet de bord : Les sommets sont traités dans l'ordre infixe.</p><h2>Principe</h2><p>C'est le parcours naturel d'un arbre.</p><p>L'algorithme est naturellement récursif.</p><p>Il traite la racine courante de l'arbre puis s'applique sur le sous-arbre gauche, puis s'applique sur le sous-arbre droit.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = [], resultat = []) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "graph": G.toDot() });
            if (!sommet.estVide()) {
                let n = G.neighbor(s);
                let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];

                G.edge(s, children[0]).set("color", "red");
                steps.push({ "line": 2, "s": sommet.label(), "graph": G.toDot() });
                this.function(G, children[0], d, e, steps, resultat);

                sommet.set("style", "filled");
                sommet.set("fillcolor", "lightgray");
                resultat.push(sommet.label());
                steps.push({ "line": 3, "s": sommet.label() });

                G.edge(s, children[1]).set("color", "red");
                steps.push({ "line": 4, "s": sommet.label(), "graph": G.toDot(), "résultat": [...resultat] });
                this.function(G, children[1], d, e, steps, resultat);
            }
            steps.push({ "line": 5, "s": sommet.label() });
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est non vide alors",
            "&nbsp;&nbsp;Parcourir, en infixe, le sous-arbre gauche de <i>s</i>",
            "&nbsp;&nbsp;Traiter le sommet <i>s</i>",
            "&nbsp;&nbsp;Parcourir, en infixe, le sous-arbre droit de <i>s</i>"
        ],
        "variables": ["s", "résultat"]
    },
    "prefixe": {
        "help": "<h1>Algorithme - Parcours préfixe</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un arbre binaire.</p><p>Effet de bord : Les sommets sont traités dans l'ordre préfixe.</p><h2>Principe</h2><p>L'algorithme est naturellement récursif.</p><p>Il s'applique sur le sous-arbre gauche, puis il traite la racine courante de l'arbre, et enfin s'applique sur le sous-arbre droit.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = [], resultat = []) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "graph": G.toDot() });
            if (!sommet.estVide()) {
                steps.push({ "line": 2 });
                sommet.set("style", "filled");
                sommet.set("fillcolor", "lightgray");
                resultat.push(sommet.label());
                let r = [...resultat];
                let n = G.neighbor(s);
                let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];

                G.edge(s, children[0]).set("color", "red");
                steps.push({ "line": 3, "s": sommet.label(), "graph": G.toDot(), "résultat": r });
                this.function(G, children[0], d, e, steps, resultat);

                G.edge(s, children[1]).set("color", "red");
                steps.push({ "line": 4, "s": sommet.label(), "graph": G.toDot() });
                this.function(G, children[1], d, e, steps, resultat);
            }
            steps.push({ "line": 5, "s": sommet.label() });
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est non vide alors",
            "&nbsp;&nbsp;Traiter le sommet <i>s</i>",
            "&nbsp;&nbsp;Parcourir, en préfixe, le sous-arbre gauche de <i>s</i>",
            "&nbsp;&nbsp;Parcourir, en préfixe, le sous-arbre droit de <i>s</i>"
        ],
        "variables": ["s", "résultat"]
    },
    "postfixe": {
        "help": "<h1>Algorithme - Parcours postfixe</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un arbre binaire.</p><p>Effet de bord : Les sommets sont traités dans l'ordre préfixe.</p><h2>Principe</h2><p>L'algorithme est naturellement récursif.</p><p>Il s'applique sur le sous-arbre gauche, puis sur le sous-arbre et enfin il traite la racine courante de l'arbre.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = [], resultat = []) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "graph": G.toDot() });
            if (!sommet.estVide()) {
                let n = G.neighbor(s);
                let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];

                G.edge(s, children[0]).set("color", "red");
                steps.push({ "line": 2, "s": sommet.label(), "graph": G.toDot() });
                this.function(G, children[0], d, e, steps, resultat);

                G.edge(s, children[1]).set("color", "red");
                steps.push({ "line": 3, "s": sommet.label(), "graph": G.toDot() });
                this.function(G, children[1], d, e, steps, resultat);

                sommet.set("style", "filled");
                sommet.set("fillcolor", "lightgray");
                resultat.push(sommet.label());
                steps.push({ "line": 4, "s": sommet.label() });
                steps.push({ "line": 5, "s": sommet.label(), "graph": G.toDot(), "résultat": [...resultat] });
            } else {
                steps.push({ "line": 5, "s": sommet.label() });
            }
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est non vide alors",
            "&nbsp;&nbsp;Parcourir, en postfixe, le sous-arbre gauche de <i>s</i>",
            "&nbsp;&nbsp;Parcourir, en postfixe, le sous-arbre droit de <i>s</i>",
            "&nbsp;&nbsp;Traiter le sommet <i>s</i>"
        ],
        "variables": ["s", "résultat"]
    },
    "recherche": {
        "help": "<h1>Algorithme - Recherche d'un élément</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme s'applique sur un arbre binaire et nécessite un élément à rechercher e.</p><p>Sortie : Vrai si l'élément e est présent dans l'arbre, Faux sinon.</p><h2>Principe</h2><p>L'algorithme est naturellement récursif.</p><p>Par convention, l'arbre vide ∆ ne contient pas l'élément. Dans ce cas, l'algorithme renvoie Faux.</p><p>L'algorithme applique un parcours infixe sur l'arbre et compare les étiquettes des noeuds jusqu'à trouver un noeud dont l'étiquette est égale à e. Dans ce cas, l'algorithme renvoie Vrai.</p><p>Dans le cas où tous les noeuds sont visités sans succès, l'algorithme renvoie Faux.</p>",
        "parameters": ["departContainer", "elementContainer"],
        "function": function (G, s, d, e, steps = [], resultat = false) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "e": e, "graph": G.toDot() });
            if (sommet.estVide()) {
                steps.push({ "line": 2 });
                steps.push({ "line": 9, "résultat": "Faux" });
                return false;
            } else {
                steps.push({ "line": 3 });
                if (sommet.label() == e) {
                    sommet.set("style", "filled");
                    sommet.set("fillcolor", "lightgray");
                    steps.push({ "line": 4 });
                    steps.push({ "line": 9, "résultat": "Vrai", "graph": G.toDot() });
                    return true;
                } else {
                    let n = G.neighbor(s);
                    let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];

                    G.edge(s, children[0]).set("color", "red");
                    steps.push({ "line": 6, "graph": G.toDot() });
                    let present_gauche = this.function(G, children[0], d, e, steps, resultat);

                    G.edge(s, children[1]).set("color", "red");
                    steps.push({ "line": 7, "s": sommet.label(), "graph": G.toDot(), "present_gauche": present_gauche });
                    let present_droit = this.function(G, children[1], d, e, steps, resultat);
                    steps.push({ "line": 8, "s": sommet.label(), "present_gauche": present_gauche, "present_droit": present_droit })

                    sommet.set("style", "filled");
                    sommet.set("fillcolor", "lightgray");
                    steps.push({ "line": 9, "résultat": (present_gauche || present_droit)?"Vrai":"Faux", "graph": G.toDot() });
                    return present_gauche || present_droit;
                }
            }
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est vide alors",
            "&nbsp;&nbsp;Renvoyer Faux",
            "Sinon Si <i>e</i> est égal l'étiquette de l'Arbre <i>s</i> alors",
            "&nbsp;&nbsp;Renvoyer Vrai",
            "Sinon",
            "&nbsp;&nbsp;present_gauche &leftarrow; rechercher <i>e</i> dans le sous-arbre gauche de <i>s</i>)",
            "&nbsp;&nbsp;present_droit &leftarrow; rechercher <i>e</i> dans le sous-arbre droit de <i>s</i>)",
            "&nbsp;&nbsp;Renvoyer present_gauche OU present_droit"
        ],
        "variables": ["e", "s", "present_gauche", "present_droit", "résultat"]
    },
    "rechercheABR": {
        "help": "<h1>Algorithme - Recherche dans un ABR</h1><h2>Prototypage</h2><p>Entrées : Cet algorithme s'applique sur un arbre binaire de recherche et nécessite un élément à rechercher e.</p><p>Sortie : Vrai si l'élément e est présent dans l'arbre, Faux sinon.</p><h2>Principe</h2><p>Par nature, les noeuds d'un ABR sont triés.<p>L'algorithme est naturellement récursif.</p><p>Il parcourt une branche de l'ABR en comparant les étiquettes succesives jusqu'à trouver l'élément e ou un arbre vide.</p>",
        "parameters": ["departContainer", "elementContainer"],
        "function": function (G, s, d, e, steps = [], resultat = false) {
            const sommet = G.node(s);
            const etiquette = parseInt(sommet.label());
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "e": e, "graph": G.toDot() });
            if (sommet.estVide()) {
                steps.push({ "line": 2 });
                resultat = false;
                steps.push({ "line": 9, "résultat": resultat });
                return false;
            } else {
                steps.push({ "line": 3 });
                if (parseInt(e) == etiquette) {
                    sommet.set("style", "filled");
                    sommet.set("fillcolor", "lightgray");
                    steps.push({ "line": 4 });
                    resultat = true;
                    steps.push({ "line": 9, "résultat": resultat, "graph": G.toDot() });
                    return true;
                } else {
                    let n = G.neighbor(s);
                    let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];
                    if (parseInt(e) < etiquette) {
                        G.edge(s, children[0]).set("color", "red");
                        steps.push({ "line": 5 });
                        steps.push({ "line": 6, "graph": G.toDot() });
                        resultat = this.function(G, children[0], d, e, steps, resultat);
                        sommet.set("style", "filled");
                        sommet.set("fillcolor", "lightgray");
                        steps.push({ "line": 9, "s": sommet.label(), "résultat": resultat, "graph": G.toDot() });
                        return resultat;
                    } else {
                        steps.push({ "line": 5 });
                        steps.push({ "line": 7 });
                        G.edge(s, children[1]).set("color", "red");
                        steps.push({ "line": 8, "graph": G.toDot() });
                        resultat = this.function(G, children[1], d, e, steps, resultat);
                        sommet.set("style", "filled");
                        sommet.set("fillcolor", "lightgray");
                        steps.push({ "line": 9, "s": sommet.label(), "résultat": resultat, "graph": G.toDot() });
                        return resultat;
                    }
                }
            }
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est vide alors",
            "&nbsp;&nbsp;Renvoyer Faux",
            "Sinon Si <i>e</i> est égal l'étiquette de l'Arbre <i>s</i> alors",
            "&nbsp;&nbsp;Renvoyer Vrai",
            "Sinon Si <i>e</i> est < l'étiquette de l'Arbre <i>s</i> alors",
            "&nbsp;&nbsp;Renvoyer rechercher <i>e</i> dans le sous-arbre gauche de <i>s</i>",
            "Sinon",
            "&nbsp;&nbsp;Renvoyer rechercher <i>e</i> dans le sous-arbre droit de <i>s</i>",
        ],
        "variables": ["e", "s", "résultat"]
    },
    "minimum": {
        "help": "<h1>Algorithme - Minimum dans un ABR</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un arbre binaire de recherche.</p><p>Sortie : Le noeud de l'arbre dont la valeur de l'étiquette est minimale.</p><h2>Principe</h2><p>Par nature, les noeuds sont triés. Le minimum d'un ABR est le noeud le plus à gauche de l'arbre.</p><p>L'algorithme est naturellement récursif.</p><p>Il parcourt les sous-arbres gauches successifs jusqu'à un noeud ne disposant pas de sous-arbre gauche.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = [], resultat = false) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "e": e, "graph": G.toDot() });
            if (sommet.estVide()) {
                steps.push({ "line": 2 });
                resultat = false;
                steps.push({ "line": 7, "résultat": resultat });
                return NaN;
            } else {
                steps.push({ "line": 3 });
                let n = G.neighbor(s);
                let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];
                if (G.node(children[0]).estVide()) {
                    sommet.set("style", "filled");
                    sommet.set("fillcolor", "lightgray");
                    steps.push({ "line": 4 });
                    resultat = sommet.label();
                    steps.push({ "line": 7, "résultat": resultat, "graph": G.toDot() });
                    return resultat;
                } else {
                    G.edge(s, children[0]).set("color", "red");
                    steps.push({ "line": 5 });
                    steps.push({ "line": 6, "graph": G.toDot() });
                    resultat = this.function(G, children[0], d, e, steps, resultat);
                    sommet.set("style", "filled");
                    sommet.set("fillcolor", "lightgray");
                    steps.push({ "line": 7, "s": sommet.label(), "résultat": resultat, "graph": G.toDot() });
                    return resultat;
                }
            }
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est vide alors",
            "&nbsp;&nbsp;Renvoyer N/A",
            "Sinon Si l'Arbre <i>s</i> n'a pas de sous-arbre gauche alors",
            "&nbsp;&nbsp;Renvoyer l'étiquette de <i>s</i>",
            "Sinon",
            "&nbsp;&nbsp;Renvoyer minimum dans le sous-arbre gauche de l'Arbre <i>s</i>"
        ],
        "variables": ["s", "résultat"]
    },
    "maximum": {
        "help": "<h1>Algorithme - Maximum dans un ABR</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un arbre binaire de recherche.</p><p>Sortie : Le noeud de l'arbre dont la valeur de l'étiquette est maximale.</p><h2>Principe</h2><p>Par nature, les noeuds sont triés. Le maximum d'un ABR est le noeud le plus à droite de l'arbre.</p><p>L'algorithme est naturellement récursif.</p><p>Il parcourt les sous-arbres droits successifs jusqu'à un noeud ne disposant pas de sous-arbre droit.</p>",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = [], resultat = false) {
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1, "s": sommet.label(), "e": e, "graph": G.toDot() });
            if (sommet.estVide()) {
                steps.push({ "line": 2 });
                resultat = false;
                steps.push({ "line": 7, "résultat": resultat });
                return NaN;
            } else {
                steps.push({ "line": 3 });
                let n = G.neighbor(s);
                let children = (n.size > 0) ? [...G.neighbor(s)] : ["", ""];
                if (G.node(children[1]).estVide()) {
                    sommet.set("style", "filled");
                    sommet.set("fillcolor", "lightgray");
                    steps.push({ "line": 4 });
                    resultat = sommet.label();
                    steps.push({ "line": 7, "résultat": resultat, "graph": G.toDot() });
                    return resultat;
                } else {
                    G.edge(s, children[1]).set("color", "red");
                    steps.push({ "line": 5 });
                    steps.push({ "line": 6, "graph": G.toDot() });
                    resultat = this.function(G, children[1], d, e, steps, resultat);
                    sommet.set("style", "filled");
                    sommet.set("fillcolor", "lightgray");
                    steps.push({ "line": 7, "s": sommet.label(), "résultat": resultat, "graph": G.toDot() });
                    return resultat;
                }
            }
        },
        "descriptif": [
            "Si l'arbre <i>s</i> est vide alors",
            "&nbsp;&nbsp;Renvoyer N/A",
            "Sinon Si l'Arbre <i>s</i> n'a pas de sous-arbre droit alors",
            "&nbsp;&nbsp;Renvoyer l'étiquette de <i>s</i>",
            "Sinon",
            "&nbsp;&nbsp;Renvoyer maximum dans le sous-arbre droit de l'Arbre <i>s</i>"
        ],
        "variables": ["s", "résultat"]
    },
    "cycle": {
        "help": "<h1>Algorithme - Existence d'un cycle</h1><h2>Prototypage</h2><p>Entrée : Cet algorithme s'applique sur un graphe.</p><p>Sortie : Vrai le graphe contient au moins un cycle, Faux sinon.</p><h2>Principe</h2><p>L'algorithme s'inspire du parcours en profondeur.</p",
        "parameters": ["departContainer"],
        "function": function (G, s, d, e, steps = []) {
            function couleur(node) {
                return (node.options.hasOwnProperty("fillcolor")) ? node.options["fillcolor"] : "white";
            }
            const sommet = G.node(s);
            sommet.set("color", "red");
            steps.push({ "line": 1 });
            steps.push({ "line": 2, "s": sommet.label(), "graph": G.toDot() });
            let p = new Pile();
            steps.push({ "line": 3, "p": xileToString(G, p) });

            p.empiler(s);
            steps.push({ "line": 4, "p": xileToString(G, p) });
            sommet.set("style", "filled");
            sommet.set("fillcolor", "lightgray");
            steps.push({ "line": 5, "graph": G.toDot() });

            while (!p.est_vide()) {
                steps.push({ "line": 6 });
                let t = p.depiler();
                G.node(t).set("color", "red");
                steps.push({ "line": 7, "t": t, "p": xileToString(G, p), "graph": G.toDot() });
                G.node(t).set("style", "filled");
                G.node(t).set("fillcolor", "black");
                G.node(t).set("fontcolor", "white");
                steps.push({ "line": 8, "graph": G.toDot() });

                for (const v of G.neighbor(t)) {
                    if (!G.node(v).estVide()){
                        steps.push({ "line": 9, "v": G.node(v).label() });
                        let c = couleur(G.node(v));
                        if (c == "lightgray") {
                            steps.push({ "line": 10 });
                            steps.push({ "line": 16, "résultat": "Vrai" });
                            return true;
                        } else if (c == "white") {
                            steps.push({ "line": 11 });
                            steps.push({ "line": 12 });
                            p.empiler(v);
                            steps.push({ "line": 13, "p": xileToString(G, p) });
                            G.node(v).set("style", "filled");
                            G.node(v).set("fillcolor", "lightgray");
                            steps.push({ "line": 8, "graph": G.toDot() });
                        } else {
                            steps.push({ "line": 14 });
                        }
                    }
                }
                steps.push({ "line": 5 });
            }
            steps.push({ "line": 15 });
            steps.push({ "line": 16, "résultat": "Faux" });
            return false;
        },
        "descriptif": [
            "L'ensemble des sommets sont coloriés en BLANC",
            "<i>p</i> &larr; Créer une pile",
            "Empiler <i>s</i> dans <i>p</i>",
            "Colorier en GRIS le sommet <i>s</i>",
            "Tant que <i>p</i> est non vide",
            "&nbsp;&nbsp;<i>t</i> &larr; Dépiler <i>p</i>",
            "&nbsp;&nbsp;Colorier en NOIR le sommet <i>t</i>",
            "&nbsp;&nbsp;Pour tous les sommets adjacents <i>v</i> de <i>t</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;Si le sommet <i>v</i> est GRIS",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Renvoyer Vrai",
            "&nbsp;&nbsp;&nbsp;&nbsp;Sinon si le sommet <i>v</i> est BLANC ",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Empiler <i>v</i> dans <i>p</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Colorier en GRIS le sommet <i>v</i>",
            "&nbsp;&nbsp;&nbsp;&nbsp;Sinon # On ne fait rien",
            "Renvoyer Faux"
        ],
        "variables": ["s", "p", "t", "v", "résultat"]
    }
}