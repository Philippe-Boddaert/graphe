class Noeud {

    static vide = new Noeud("", { "style": "invis", "label": "∆" });

    constructor(id, options = {}) {
        this.id = id;
        this.options = options;
    }

    label() {
        if (this.options.hasOwnProperty("style") && this.options["style"] == "invis") {
            return "∆";
        }
        if (this.options.hasOwnProperty("label")) {
            return this.options["label"];
        }
        return this.id;
    }

    set(option, value) {
        this.options[option] = value;
    }

    toDot() {
        let chaine = "\"" + this.id + "\"";
        let configuration = [];

        Object.keys(this.options).forEach(o => {
            configuration.push(o + "=\"" + this.options[o] + "\"");
        });

        return chaine + ((configuration.length > 0) ? ("[" + configuration.join(", ") + "]") : "");
    }

    estVide() {
        return (this.options.hasOwnProperty("style") && this.options["style"] == "invis")
    }
}

class Edge {

    static vide = new Edge("", "", { "style": "invis" });

    constructor(source, destination, options = {}) {
        this.source = source;
        this.destination = destination;
        this.options = options;
    }

    set(option, value) {
        this.options[option] = value;
    }

    value(){
        if (this.options.hasOwnProperty("label")){
            return parseInt(this.options["label"], 10);
        }
        return NaN;
    }
    
    toDot(connector) {
        let chaine = "\"" + this.source + "\"" + connector + "\"" + this.destination + "\"";
        let configuration = [];

        Object.keys(this.options).forEach(o => {
            configuration.push(o + "=\"" + this.options[o] + "\"");
        });

        return chaine + ((configuration.length > 0) ? ("[" + configuration.join(", ") + "]") : "");
    }
}

class Graph {

    constructor(nodes = new Set(), edges = new Set(), isOriented = false, options = { "nodes" : {"ordering" : "out"}}) {
        this.__nodes = {};
        nodes.forEach(n => { this.__nodes[n] = new Noeud(n) });
        this.__edges = {};
        edges.forEach(e => { this.__edges[e[0] + "-" + e[1]] = new Edge(e[0], e[1]) });
        this.__isOriented = isOriented;
        this.__options = options;
    }

    visibleNodes() {
        return new Set(Object.values(this.__nodes).filter(n => !(n.options.hasOwnProperty("style") && n.options["style"] == "invis")));
    }

    nodes() {
        return new Set(Object.values(this.__nodes));
    }

    edges() {
        return new Set(Object.values(this.__edges));
    }

    node(id) {
        if (id == "")
            return Noeud.vide;
        return this.__nodes[id];
    }

    edge(s, d) {
        if (s == "" || d == "")
            return Edge.vide;
        let e = this.__edges[s + "-" + d];
        if (e === undefined && !this.__isOriented){
            return this.__edges[d + "-" + s];
        }
        return e;
    }

    toDot() {
        let chaine = (this.__isOriented) ? "digraph" : "graph";
        chaine += " G {\n";
        if (this.__options.hasOwnProperty("nodes")) {
            chaine += "\tnode[" + Object.keys(this.__options.nodes).map(p => { return p + "=" + this.__options.nodes[p]; }).join(', ') + "];\n";
        }
        this.nodes().forEach(n => {
            chaine += "\t" + n.toDot() + ";\n";
        });
        const connector = (this.__isOriented) ? " -> " : " -- ";

        this.edges().forEach(e => {
            chaine += "\t" + e.toDot(connector) + ";\n";
        });
        return chaine + "}";
    }

    static fromDot(dot) {
        let lines = dot.split('\n');
        const isOriented = lines[0].trim().startsWith('digraph');
        const separator = (isOriented) ? " -> " : " -- ";
        const nodes = {};
        const edges = {};
        const options = { "nodes": {}, "edges": {} };
        const g = new Graph(new Set(), new Set(), isOriented, options);

        for (let i = 1; i < lines.length - 1; i++) {
            let line = lines[i].replaceAll("\"", "").replace(";", "").trim();
            if (line.length > 0) {
                if (!(line.startsWith("node") || line.startsWith("edge"))) {
                    if (line.indexOf(separator) > 0) {
                        let elements = line.split(separator);
                        let s = elements[0].trim();
                        let d = elements[1].trim();
                        let o = {};
                        if (elements[1].indexOf("[") >= 0) {
                            d = elements[1].substring(0, elements[1].indexOf("[")).trim();
                            let e = elements[1].substring(elements[1].indexOf("[") + 1, elements[1].indexOf("]")).split(",");
                            e.forEach(function (el) {
                                let option = el.split("=");
                                o[option[0].trim()] = option[1].trim();
                            });
                        }
                        edges[s + "-" + d] = new Edge(s, d, o);
                    } else {
                        if (line.indexOf("[") >= 0) {
                            let o = {};
                            let e = line.substring(line.indexOf("[") + 1, line.indexOf("]")).split(",");
                            e.forEach(function (el) {
                                let option = el.split("=");
                                o[option[0].trim()] = option[1].trim();
                            });
                            nodes[line.substring(0, line.indexOf("[")).trim()] = new Noeud(line.substring(0, line.indexOf("[")).trim(), o);
                        } else {
                            nodes[line] = new Noeud(line);
                        }
                    }
                } else if (line.startsWith("node")) {
                    let elements = line.substring(line.indexOf("[") + 1, line.indexOf("]")).split(",");
                    elements.forEach(function (e) {
                        let option = e.split("=");
                        options["nodes"][option[0].trim()] = option[1].trim();
                    });
                }
            }
        }
        g.__nodes = nodes;
        g.__edges = edges;
        return g;
    }

    neighbor(id) {
        let e = new Set();

        this.edges().forEach(edge => {
            if (!this.__isOriented) {
                if (edge.source == id) {
                    e.add(edge.destination);
                } else if (edge.destination == id) {
                    e.add(edge.source);
                }
            } else {
                if (edge.source == id) {
                    e.add(edge.destination);
                }
            }
        });
        return e;
    }
}