class Pile {
    /**
    Pile : structure de données fondée sur le principe « dernier arrivé, premier sorti ».
    **/
    __structure;

    constructor() {
        this.__structure = [];
    };

    toString() {
        return "[" + this.__structure.toString() + "]";
    }

    est_vide() {
        /**
        indique si la Pile est vide.
        :return: (bool) Renvoie True si elle est vide, False sinon.
        :Exemple:
            >>> p = Pile()
            >>> p.est_vide()
            True
            >>> p.empiler("A")
            >>> p.est_vide()
            False
        **/
        return this.__structure.length == 0;
    };

    empiler(element) {
        /**
        empile un élément au sommet de la Pile
        :param: (object) un élément à empiler
        :return: (None)
        :Exemple:
            >>> p = Pile()
            >>> print(p)
            []
            >>> p.empiler('A')
            >>> print(p)
            ['A']
            >>> p.empiler('B')
            >>> print(p)
            ['A', 'B']
        **/
        this.__structure.push(element);
    };

    depiler() {
        /**
        dépile l'élément au sommet de la Pile
        :return: (object) l'élément au sommet de la Pile
        :Exemple:
            >>> p = Pile()
            >>> p.empiler('A')
            >>> p.depiler()
            'A'
            >>> p.depiler()
            Traceback (most recent call last):
            ...
            ValueError: La pile est vide
        **/
        if (this.est_vide()) {
            throw "La pile est vide";
        }
        return this.__structure.pop();
    };

    sommet() {
        /**
        retourne l'élément au sommet de la pile, sans le dépiler
        :return: (object) l'élément au sommet de la Pile
        Equivalent à :
            >> element = pile.deplier()
            >> pile.empiler(element)
        :Exemple:
            >>> p = Pile()
            >>> p.empiler('A')
            >>> p.sommet()
            'A'
            >>> p.est_vide()
            False
            >>> p.depiler()
            'A'
            >>>
        **/
        if (this.est_vide()) {
            throw "La pile est vide";
        }
        return this.__structure[this.__structure.length - 1];
    };
};

class File {
    /**
    File : structure de données fondée sur le principe « permier arrivé, premier sorti ».
    **/
    __structure;

    constructor() {
        this.__structure = [];
    };

    toString() {
        return "[" + this.__structure.toString() + "]";
    }

    est_vide() {
        /**
        indique si la File est vide.
        :return: (bool) Renvoie True si elle est vide, False sinon.
        :Exemple:
            >>> f = File()
            >>> f.est_vide()
            True
            >>> f.enfiler("A")
            >>> f.est_vide()
            False
        **/
        return this.__structure.length == 0;
    };

    enfiler(element) {
        /**
        enfile un élément en queue de la File
        :param: (object) un élément à enfiler
        :return: (None)
        :Exemple:
            >>> f = File()
            >>> print(f)
            []
            >>> f.enfiler('A')
            >>> print(f)
            ['A']
            >>> f.enfiler('B')
            >>> print(f)
            ['A', 'B']
        **/
        this.__structure.push(element);
    };

    defiler() {
        /**
        défile l'élément en tête de la File
        :return: (object) l'élément en tête de la File
        :Exemple:
            >>> f = File()
            >>> f.enfiler('A')
            >>> f.defiler()
            'A'
            >>> f.defiler()
            Traceback (most recent call last):
            ...
            ValueError: La file est vide
        **/
        if (this.est_vide()) {
            throw "La file est vide";
        }
        return this.__structure.shift();
    };

    queue() {
        /**
        retourne l'élément en queue de la file
        :return: (object) l'élément en queue de la File
        :Exemple:
            >>> f = File()
            >>> f.enfiler('A')
            >>> f.queue()
            'A'
            >>> f.est_vide()
            False
            >>> f.defiler()
            'A'
            >>>
        **/
        if (this.est_vide()) {
            throw "La file est vide";
        }
        return this.__structure[this.__structure.length - 1];
    };

    tete() {
        /**
        retourne l'élément en tête de la file, sans le défiler
        :return: (object) l'élément en tête de la File
        :Exemple:
            >>> f = File()
            >>> f.enfiler('A')
            >>> f.tete()
            'A'
            >>> f.est_vide()
            False
            >>> f.defiler()
            'A'
            >>>
        **/
        if (this.est_vide()) {
            throw "La file est vide";
        }
        return this.__structure[0];
    };
};