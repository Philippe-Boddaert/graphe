const popup = document.querySelector("#popup");
const algorithmes = document.querySelector("#algorithmes");
const algorithme = document.querySelector("#algorithme tbody");
const variables = document.querySelector("#variables tbody");
const dot = document.querySelector("#dot");
const engine = document.querySelector("#engine");

const searchParams = new URLSearchParams(document.location.search);

if (searchParams.has("g")){
    dot.value = decode(searchParams.get("g"));
}

if (searchParams.has("m")){
    engine.value = searchParams.get("m");
}

let g = null;
let steps = [];
let marker = 0;
let resultat = [];
let viz = null;

function encode(unencoded) {
    return encodeURIComponent(unencoded).replace(/'/g, "%27").replace(/"/g, "%22");
}
function decode(encoded) {
    return decodeURIComponent(encoded.replace(/\+/g, " "));
}

function dropHandler(ev) {
    popup.style.display = "none";
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    if (ev.dataTransfer.items) {
        // Use DataTransferItemList interface to access the file(s)
        [...ev.dataTransfer.items].forEach((item, i) => {
        // If dropped items aren't files, reject them
        if (item.kind === "file") {
            const file = item.getAsFile();
            if (file.name.endsWith(".dot")){
                file.text().then(texte => {
                    dot.value = texte;
                    const e = new Event("change");
                    dot.dispatchEvent(e);
                });
            } else {
                document.querySelector("#popup-message").innerHTML = "Seuls des fichiers .dot sont autorisés.";
                popup.style.display = "flex";
            }
        }
        });
    } else {
        // Use DataTransfer interface to access the file(s)
        [...ev.dataTransfer.files].forEach((file, i) => {
        console.log(`… file[${i}].name = ${file.name}`);
        });
    }
}

function dragOverHandler(ev) {
    document.querySelector("#popup-message").innerHTML = "Relachez pour importer le graphe au format DOT.";
    popup.style.display = "flex";

    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
}

function update() {
    g = Graph.fromDot(dot.value);

    document.querySelector("#viz").innerHTML = "";
    document.querySelector("#viz").appendChild(viz.renderSVGElement(dot.value, { 'engine': engine.selectedOptions[0].value }));

    document.querySelector("#depart").innerHTML = "<option value='' selected>Aléatoire</option>";
    document.querySelector("#arrivee").innerHTML = "";

    g.visibleNodes().forEach(node => {
        let option = document.createElement("option");
        option.value = node.id;
        option.innerHTML = node.label();
        document.querySelector("#depart").appendChild(option);
        let clone = option.cloneNode();
        clone.innerHTML = node.label();
        document.querySelector("#arrivee").appendChild(clone);
    });
}

function launchAlgo() {
    g = Graph.fromDot(dot.value);
    steps = [];
    resultat = [];
    marker = 0;
    const algo = algos[algorithmes.selectedOptions[0].value];
    let s = document.querySelector("#depart").selectedOptions[0].value;
    let d = document.querySelector("#arrivee").selectedOptions[0].value;
    let e = document.querySelector("#element").value;

    if (s == '') {
        s = [...g.visibleNodes()][Math.floor(Math.random() * g.visibleNodes().size)].id;
    }
    algo.function(g, s, d, e, steps);
}

function start() {
    document.querySelector("#viz").innerHTML = "";
    document.querySelector("#viz").appendChild(viz.renderSVGElement(dot.value, { 'engine': engine.selectedOptions[0].value }));
    marker = 0;
    algorithme.querySelector(".marker").classList.remove("marker");
    algorithme.children[0].children[0].classList.add("marker");
    algos[algorithmes.selectedOptions[0].value].variables.forEach(v => {
        document.querySelector("#v_" + v).innerHTML = "";
    });
}

function getPreviousValue(variable){
    let indice = marker;
    while (indice >= 0 && !steps[indice].hasOwnProperty(variable)){
        indice--;
    }
    if (indice < 0)
        return "";
    return steps[indice][variable];
}

function previous() {
    if (marker >= 0){
        marker = Math.max(marker - 2, 0);
        algorithme.querySelector(".marker").classList.remove("marker");
        algorithme.children[steps[marker].line].children[0].classList.add("marker");
        algos[algorithmes.selectedOptions[0].value].variables.forEach(v => {
                document.querySelector("#v_" + v).innerHTML = getPreviousValue(v);
        });
        let value = getPreviousValue("graph");
        document.querySelector("#viz").innerHTML = "";
        document.querySelector("#viz").appendChild(viz.renderSVGElement((value=="")?dot.value:value, { 'engine': engine.selectedOptions[0].value }));
        
        marker++;
    }
}

function next() {
    if (marker < steps.length) {
        algorithme.querySelector(".marker").classList.remove("marker");
        algorithme.children[steps[marker].line].children[0].classList.add("marker");
        algos[algorithmes.selectedOptions[0].value].variables.forEach(v => {
            if (steps[marker].hasOwnProperty(v)) {
                document.querySelector("#v_" + v).innerHTML = steps[marker][v];
            }
        });
        if (steps[marker].hasOwnProperty("graph")) {
            document.querySelector("#viz").innerHTML = "";
            document.querySelector("#viz").appendChild(viz.renderSVGElement(steps[marker]["graph"], { 'engine': engine.selectedOptions[0].value }));
        }
        marker++;
    }
}

function end() {
    while (marker < steps.length) {
        next();
    }
}

function updateAlgo() {
    algorithme.innerHTML = "<tr><td class='marker'></td><td colspan=2>Début</td></tr>";
    variables.innerHTML = "";
    const algo = algos[algorithmes.selectedOptions[0].value];
    algo.descriptif.forEach((ligne, index) => {
        let tr = document.createElement('tr');
        let td = document.createElement('td');
        td.innerHTML = "";
        tr.appendChild(td);
        td = document.createElement('td');
        td.innerHTML = index + 1;
        tr.appendChild(td);
        td = document.createElement('td');
        td.innerHTML = ligne;
        tr.appendChild(td);
        algorithme.appendChild(tr);
    });
    let tr = document.createElement('tr');
    let td = document.createElement('td');
    td.innerHTML = "";
    tr.appendChild(td);
    td = document.createElement('td');
    td.colspan = 2;
    td.innerHTML = "Fin";
    tr.appendChild(td);
    algorithme.appendChild(tr);

    document.querySelector("#departContainer").classList.add("hide");
    document.querySelector("#arriveeContainer").classList.add("hide");
    document.querySelector("#elementContainer").classList.add("hide");

    algo.parameters.forEach(p => {
        document.querySelector("#" + p).classList.remove("hide");
    });

    algo.variables.forEach(v => {
        let tr = document.createElement('tr');
        let td = document.createElement('th');
        td.innerHTML = "<i>" + v + "</i>";
        tr.appendChild(td);
        td = document.createElement('td');
        td.innerHTML = "";
        td.id = "v_" + v;
        tr.appendChild(td);
        variables.appendChild(tr);
    });

}

popup.addEventListener('click', function () {
    this.style.display = "none";
});

function helpGraph() {
    return document.querySelector("#helpGraph").innerHTML;
}

function helpCommand() {
    return document.querySelector("#helpCommand").innerHTML;
}

function helpAlgo() {
    return algos[algorithmes.selectedOptions[0].value].help;
}

document.querySelectorAll(".help").forEach(e => {
    e.addEventListener('click', function () {
        document.querySelector("#popup-message").innerHTML = eval(e.dataset["action"]);
        popup.style.display = "flex";
    });
});

dot.addEventListener('change', function () { update(); launchAlgo() });
document.querySelector("#depart").addEventListener('change', function () { launchAlgo(); start() });
document.querySelector("#arrivee").addEventListener('change', function () { launchAlgo(); start() });
document.querySelector("#element").addEventListener('change', function () { launchAlgo(); start() });
document.querySelector("#start").addEventListener('click', start);
document.querySelector("#previous").addEventListener('click', previous);
document.querySelector("#next").addEventListener('click', next);
document.querySelector("#end").addEventListener('click', end);
document.querySelector("#link").addEventListener('click', function(){
    // Get the text field
    let link = document.location.origin + document.location.pathname + "?g=" + encode(dot.value) + "&m=" + engine.value + "&algo=" + algorithmes.value + "&s=" + document.querySelector("#depart").value + "&a=" + document.querySelector("#arrivee").value;

    // Copy the text inside the text field
    navigator.clipboard.writeText(link);

    // Alert the copied text
    alert("Le lien vers la page a été copié dans le presse-papier.");
});
algorithmes.addEventListener('change', function () { updateAlgo(); launchAlgo(); start(); });

document.querySelectorAll(".menu").forEach(function (element) {
    element.addEventListener('click', function () {
        document.querySelector(this.dataset["link"]).classList.toggle('hide');
        if (this.innerText == "<") {
            this.innerText = ">";
        } else {
            this.innerText = "<";
        }
    });
});
Viz.instance().then(function (v) {
    viz = v;
    update();
    if (searchParams.has("s")){
        document.querySelector("#depart").value = searchParams.get("s");
    }
    if (searchParams.has("a")){
        document.querySelector("#arrivee").value = searchParams.get("a");
    }
    if (searchParams.has("algo")){
        algorithmes.value = searchParams.get("algo");
    }
    updateAlgo();
    launchAlgo();
});